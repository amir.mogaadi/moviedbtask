import React from 'react';
import './App.css';
import LandingPage from './components/LandingPage/LandigPage'
import { Switch,Route, BrowserRouter } from 'react-router-dom';
import MovieDetailPage from './components/LandingPage/MovieDetailPage';
import NavBar from './components/NavBar/NavBar';
import SearchMovie from './components/LandingPage/SearchMovie';

function App() {
  return (
    <>
      <NavBar/>
      <div style={{ paddingTop: '78px', minHeight: 'calc(100vh - 80px)' }}>
      <BrowserRouter>
       <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/movie/:movieId" component={MovieDetailPage} />
        <Route exact path="/search" component={SearchMovie} />
       </Switch>
       </BrowserRouter>
       </div>
    </>
  );
}

export default App;
