import React, { useEffect, useState } from 'react';
import { API_URL, API_KEY, IMAGE_URL } from '../../Config';
import { Typography } from 'antd';
import MainImage from '../Sections/MainImage';
import GridCard from '../Sections/GridCard';

const { Title } = Typography

function LandingPage() {

    const [Movies, setMovies] = useState([])
    const [Series, setSeries] = useState([])
    const [Family, setFamily] = useState([])
    const [Doc, setDoc] = useState([])

    useEffect(() => {

        fetch(`${API_URL}movie/popular?api_key=${API_KEY}`)
            .then(response => response.json())
            .then(response => {
                setMovies(response.results)
            })

        //fetch Series
        fetch(`${API_URL}tv/popular?api_key=${API_KEY}`)
            .then(response => response.json())
            .then(response => {

                setSeries(response.results)
            })

        //fetch Family
        fetch(`${API_URL}discover/tv?api_key=${API_KEY}&with_genres=10751`)
            .then(response => response.json())
            .then(response => {
                setFamily(response.results)
            })
        //fetch Doc
        fetch(`${API_URL}discover/tv?api_key=${API_KEY}&with_genres=99`)
            .then(response => response.json())
            .then(response => {
                setDoc(response.results)
            })
    }, [])


    return (

        <div style={{ width: '100%', margin: 0 }}>
            {/* {mainImage} */}

            {Movies[0] &&
                <MainImage image={`${IMAGE_URL}w1280${Movies[0].backdrop_path}`}
                    title={Movies[0].original_title}
                    text={Movies[0].overview} />
            }

            {/* {body} */}
            <div style={{ width: '85%', margin: '1rem auto' }}>
                <Title style={{ textAlign: 'left' }} level={2}>Movies by latest</Title>
                <hr />
                <div className="row" >
                    {Movies && Movies.map((movie, index) => (
                        <React.Fragment key={index}>
                            <GridCard
                                image={movie.poster_path && `${IMAGE_URL}w500${movie.poster_path}`}
                                movieId={movie.id}
                            />
                        </React.Fragment>

                    ))}
                </div>
            </div>

            {/* tv show */}
            <div style={{ width: '85%', margin: '1rem auto' }}>
                <Title style={{ textAlign: 'left' }} level={2}>Popular Series</Title>
                <hr />
                <div className="row" >
                    {Series && Series.map((serie, index) => (
                        <React.Fragment key={index}>
                            <GridCard
                                image={serie.poster_path && `${IMAGE_URL}w500${serie.poster_path}`}
                                movieId={serie.id}
                            />
                        </React.Fragment>
                    ))}
                </div>
            </div>

            {/* Family */}
            <div style={{ width: '85%', margin: '1rem auto' }}>
                <Title style={{ textAlign: 'left' }} level={2}>Family</Title>
                <hr />
                <div className="row" >
                    {Family && Family.map((family, index) => (
                        <React.Fragment key={index}>
                            <GridCard
                                image={family.poster_path && `${IMAGE_URL}w500${family.poster_path}`}
                                movieId={family.id}
                            />
                        </React.Fragment>
                    ))}
                </div>
            </div>

            {/* Doc */}
            <div style={{ width: '85%', margin: '1rem auto' }}>
                <Title style={{ textAlign: 'left' }} level={2}>Documentary</Title>
                <hr />
                <div className="row" >
                    {Doc && Doc.map((doc, index) => (
                        <React.Fragment key={index}>
                            <GridCard
                                image={doc.poster_path && `${IMAGE_URL}w500${doc.poster_path}`}
                                movieId={doc.id}
                            />
                        </React.Fragment>
                    ))}
                </div>
            </div>
            <div>
            </div>
        </div>

    )
}
export default LandingPage;