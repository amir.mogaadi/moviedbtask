import React, { useEffect, useState } from 'react';
import { API_URL, API_KEY, IMAGE_URL } from '../../Config';
import MainImage from '../Sections/MainImage'
import { Typography, Card } from 'antd';
import YouTube from '@u-wave/react-youtube';

const { Title } = Typography

export default function MovieDetailPage(props) {

  const [Movie, setMovie] = useState()
  const [Video, setVideo] = useState()

  useEffect(() => {
    
    const movieId = props.match.params.movieId
    //Get Video 
    fetch(`${API_URL}movie/${movieId}/videos?api_key=${API_KEY}&language=en-US`)
      .then(response => response.json())
      .then(response => {
        setVideo(response)
      })
     //Get Movie details
      fetch(`${API_URL}movie/${movieId}?api_key=${API_KEY}`)
      .then(response => response.json())
      .then(response => {
        setMovie(response)
      })

  }, [])


  return (
    <div style={{ width: '99%', margin: '10px', justifyContent: 'center' }}>
      <div className='row'>
        {Movie && <div className='col-sm-6'>
          <div style={{ background: '#ECECEC', padding: '20px' }}>
            <Title level={2} style={{ textAlign: 'left' }}>Movie Title : {Movie.original_title}</Title>
            <Card style={{ marginBottom: '15px' }}>
              <p style={{ width: '100%', textAlign: 'justify' }}><span style={{ fontWeight: 'bold', fontSize: '2rem' }}>Overview : </span>{Movie.overview}</p>
              <p style={{ width: '100%', textAlign: 'justify' }}><span style={{ fontWeight: 'bold', fontSize: '2rem' }}>Release Date : </span>{Movie.release_date}</p>
              <p style={{ width: '100%', textAlign: 'justify' }}><span style={{ fontWeight: 'bold', fontSize: '2rem' }}>Runtime : </span>{Movie.runtime} min</p>
              <p style={{ width: '100%', textAlign: 'initial' }}><span style={{ fontWeight: 'bold', fontSize: '2rem' }}>Genres :  </span></p>
              {Movie.genres.map(m => (<React.Fragment key={m.id}><p style={{ textAlign: 'left' }} key={m.id}>{m.name}</p></React.Fragment>))}
            </Card>

            <Card>
              <p style={{ textAlign: 'left', fontWeight: 'bold', fontSize: '2rem' }} level={4}>Production Countries : </p>
             
              {Movie.production_countries.map(p =>(
              <React.Fragment key={p.iso_3166_1}><p style={{ textAlign: 'left' }}>{p.name}</p></React.Fragment>) 
              )}

              <p style={{ textAlign: 'left', fontWeight: 'bold', fontSize: '2rem' }} level={4}>Production Companies : </p>
             
              {Movie.production_companies.map(c => (
                <React.Fragment key={c.id}>
                  <p style={{ textAlign: 'left' }}>{c.name}</p>
                  <img style={{ width: '74%', height: '112px' }} alt="pc" src={c.logo_path && `${IMAGE_URL}w500${c.logo_path}`} />
                  </React.Fragment >))
              }
            </Card>
          </div>
        </div>}

        <div className='col-sm-6'>
          {/* {mainImage} */}
          {Movie &&
            <MainImage image={`${IMAGE_URL}w1280${Movie.backdrop_path}`}
              title={Movie.original_title}
              text={Movie.overview} />
          }
          <button style={{ marginTop: '20px' }} type="button" className="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Watch Trailer</button>
          <div id="myModal" className="modal fade" role="dialog">
            <div className="modal-dialog modal-lg">

              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>

                </div>
                <div className="modal-body">
                 {Video ?
                  <YouTube width='99%' height='352px'
                    video={Video.results[0].key} 
                  />
                  :null
                }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
