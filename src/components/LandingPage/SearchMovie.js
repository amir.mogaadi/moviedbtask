import React, { useState, useEffect } from 'react';
import GridCard from '../Sections/GridCard';
import { API_KEY, API_URL, IMAGE_URL } from '../../Config';
import { Typography } from 'antd';

const { Title } = Typography

export default function SearchMovie() {
    
    const [Movies, setMovies] = useState([])

    /*Init*/
        useEffect(() => {
        searchMovie('woman')
        }, [])

        const searchMovie = (serachKey) => {
        fetch(`${API_URL}search/movie?api_key=${API_KEY}&query=` + serachKey)
            .then(response => response.json())
            .then(response => {
                setMovies(response.results)
            })
     }

    const searchChangeHandler = (event) => {
        const searchK = event.target.value
        searchMovie(searchK)
    }


    return (
        <div >
            <Title style={{ marginTop: '5%', marginLeft: '10%' }} level={2}>SEARCH</Title>
            <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '90px' }}>
                <input style={{
                    fontSize: 17, display: 'block', width: '80%', marginTop: 30, borderRadius: 10,
                    paddingLeft: 10, marginRight: 30, marginBottom: 5, outline: 'none'
                }}
                    placeholder="Enter name of movie"
                    onChange={searchChangeHandler} />
            </div>
            <Title style={{marginLeft: '2%' }}  level={4}>Search result</Title>
            {Movies && Movies.map((movie, index) => (
                <React.Fragment key={index}>
                    <GridCard
                        image={movie.poster_path && `${IMAGE_URL}w500${movie.poster_path}`}
                        movieId={movie.id}
                    />
                </React.Fragment>
            ))}
        </div>
    );
}
