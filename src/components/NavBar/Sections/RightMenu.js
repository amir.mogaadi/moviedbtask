import React from 'react';
import { Menu } from 'antd';

function RightMenu(props) {
 
    return (
      <Menu mode={props.mode}>
        <Menu.Item key="mail">
          <a href="/search">Search</a>
        </Menu.Item>
      </Menu>
    )
  
}

export default RightMenu;

