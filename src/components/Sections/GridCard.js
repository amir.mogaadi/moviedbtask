import React from 'react';

export default function GridCard(props) {
 
  return (
    <div className="col-md-2" style={{justifyContent:'space-between',marginBottom:'10px'}}>
      <div style={{position:'relative'}}>
         <a href={`/movie/${props.movieId}`}>
           <img style={{width:'100%',height:'320px'}} alt='card' src={props.image} />
         </a>
      </div>

    </div>
  );
}
